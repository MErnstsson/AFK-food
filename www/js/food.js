var app = angular.module('foodApp', []);

app.factory('FoodService', function ($http, $q) {
	return {
		getFood: function() {
			return $http.get('/api/food')
			.then(function(response) {
				if (typeof response.data === 'object') {
					return response.data;
				} else {
					return $q.reject(response.data);
				}
			}, function(response) {
				return $q.reject(response.data);
			});
		}
	};
});

app.controller('FoodController', function($scope, FoodService, $timeout) {
	var loadTime = 1000, //Load the data every second
	errorCount = 0, //Counter for the server errors
	loadPromise; //Pointer to the promise created by the Angular $timout service
	var FoodPromise = function() {
		FoodService.getFood()
		.then(function(data) {
			$scope.orders = data
			errorCount = 0;
			nextLoad();
		}, function(error) {
			console.log('error', error);
			nextLoad(++errorCount * 2 * loadTime);
		});
	};

	var cancelNextLoad = function() {
		$timeout.cancel(loadPromise);
	};

	var nextLoad = function(mill) {
		mill = mill || loadTime;

		//Always make sure the last timeout is cleared before starting a new one
		cancelNextLoad();
		loadPromise = $timeout(FoodPromise, mill);
	};

	// will keep polling and leak memory
	$scope.$on('$destroy', function() {
		cancelNextLoad();
	});

	FoodPromise()

	//Always clear the timeout when the view is destroyed, otherwise it will keep polling and leak memory
	$scope.$on('$destroy', function() {
		cancelNextLoad();
	});

	$scope.data = "";
});
